const RPCServer = require('../server');
const {WebSocket, Server} = require('mock-socket');

test('should initialize without crashing', next => {
    const server = new Server('ws://localhost:80');
    const rpcServer = new RPCServer(server); 
    server.stop(next);
});

test('can create a realm', next => {
    const server = new Server('ws://localhost:80');
    const rpcServer = new RPCServer(server); 
    const myRealm = rpcServer.realm('realm');
    expect(myRealm.id).toBe('realm');
    server.stop(next);
});

test('creating same realm twice returns first realm', next => {
    const server = new Server('ws://localhost:80');
    const rpcServer = new RPCServer(server); 
    const myRealm = rpcServer.realm('realm');
    const myRealmAgain = rpcServer.realm('realm');
    expect(myRealm).toEqual(myRealmAgain);
    server.stop(next);
});

test('can register a method', next => {
    const server = new Server('ws://localhost:80');
    const rpcServer = new RPCServer(server); 
    const myRealm = rpcServer.realm('realm');
    const myRealmAgain = rpcServer.realm('realm');
    expect(myRealm).toEqual(myRealmAgain);
    server.stop(next);
});

test('can call a registered method', next => {
    const server = new Server('ws://localhost:80');
    const rpcServer = new RPCServer(server); 
    const myRealm = rpcServer.realm('realm.{id}');
    myRealm.register('myMethod', ({conn, params, realmData}) => {
        expect(realmData.id).toEqual('realm.1');
        expect(params).toEqual(['param1']);
        server.stop(next);
    });

    rpcServer.callMethod('realm.1', 'myMethod', 'param1');
});

test('can broadcast a message', next => {
    const server = new Server('ws://localhost:80');
    const rpcServer = new RPCServer(server);

    rpcServer._send = jest.fn();

    const myRealm = rpcServer.realm('realm.{id}');

    //generate empty listener for topic event
    rpcServer.callMethod('realm.1', 'rpc.on', 'topic');

    myRealm.register('myMethod', ({conn, params, realmData}) => {
        realmData.emit('topic', 'success');
    });

    rpcServer.callMethod('realm.1', 'myMethod', 'param1');

    expect(rpcServer._send).toBeCalled();
    server.stop(next);
});