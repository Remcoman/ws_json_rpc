const RPCClient = require('..').default;
const {WebSocket, Server} = require('mock-socket');

test('should require socket', () => {
    expect(() => {
        const client = new RPCClient();
    }).toThrowError();
});

test('should crash on invalid socket', () => {
    expect(() => {
        const client = new RPCClient({send : 1});
    }).toThrowError();
});

test('initializes with valid socket', () => {
    const socket = new WebSocket('ws://localhost:8080');
    const client = new RPCClient(socket);
});

test('can create realm', () => {
    const socket = new WebSocket('ws://localhost:8080');
    const client = new RPCClient(socket);
    client.realm('test');
});

test('can notify with params', (next) => {
    const server = new Server('ws://localhost:8080');
    server.on('message', message => {
        expect(message).toEqual(`{"jsonrpc":"2.0","realm":"test","method":"something","id":null,"params":["arg1","arg2"]}`);
        server.stop(next);
    })

    const socket = new WebSocket('ws://localhost:8080');
    const client = new RPCClient(socket);
    const realm = client.realm('test');
    realm.notify('something', 'arg1', 'arg2');
});

test('can call method', (next) => {
    const server = new Server('ws://localhost:8080');
    server.on('message', message => {
        const data = JSON.parse(message);
        expect(data.method).toEqual('something');
        expect(JSON.stringify(data.params)).toBe(`["arg1","arg2"]`);
        expect(data.id).toEqual(expect.anything());
        server.stop(next);
    });

    const socket = new WebSocket('ws://localhost:8080');
    const client = new RPCClient(socket);
    const realm = client.realm('test');
    realm.call('something', 'arg1', 'arg2');
});

test('call can timeout', (next) => {
    const server = new Server('ws://localhost:8080');

    const socket = new WebSocket('ws://localhost:8080');
    const client = new RPCClient(socket, {callTimeout : 500});
    const realm = client.realm('test');
    realm.call('something', 'arg1', 'arg2')
        .catch(e => {
            server.stop(next);
        });
});

test('can subscribe to a event', (next) => {
    const server = new Server('ws://localhost:8080');
    server.on('message', message => {
        const data = JSON.parse(message);
        expect(data.method).toEqual('rpc.on');
        expect(JSON.stringify(data.params)).toBe(`["event"]`);
        expect(data.id).toEqual(expect.anything());
        server.stop(next);
    });

    const socket = new WebSocket('ws://localhost:8080');
    const client = new RPCClient(socket);
    const realm = client.realm('test');
    realm.subscribe('event', () => {

    });
});

test('calls handler for event', (next) => {
    const server = new Server('ws://localhost:8080');
    server.on('message', message => {
        const data = JSON.parse(message);

        //confirm subscription
        server.send(JSON.stringify({
            jsonrpc : '2.0',
            realm : 'test',
            id : data.id,
            result : null
        }));

        //send event
        setTimeout(() => {
            server.send(JSON.stringify({
                jsonrpc : '2.0',
                realm : 'test',
                topic : 'event',
                result : 'ok',
                id : null
            }));
        }, 50);
    });

    const socket = new WebSocket('ws://localhost:8080');
    const client = new RPCClient(socket);
    const realm = client.realm('test');
    realm.subscribe('event', result => {
        expect(result).toBe('ok');
        server.stop(next);
    });
});