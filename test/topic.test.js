const TopicTree = require('../server/topic');

it('can create a topic tree', () => {
    const tree = new TopicTree();
});

it('can add a topic', () => {
    const tree = new TopicTree();
    tree.add('user.{id}', 'value1');

    const found = tree.find('user.remco');

    expect(found).toBeTruthy();
    expect(found.firstValue).toBe('value1');
    expect(found.vars.get('id')).toBe('remco');
});

it('cannot add more then one value to a branch by default', () => {
    const tree = new TopicTree({allowMultipleValues : false});
    tree.add('user', 'value1');

    expect(() => {
        tree.add('user', 'value2');
    }).toThrow();
});

it('can add more values when allowMultipleValues = true', () => {
    const tree = new TopicTree({allowMultipleValues : true});
    tree.add('user', 'value1');
    tree.add('user', 'value2');

    const found = tree.find('user');

    expect(found.values.has('value1')).toBeTruthy();
    expect(found.values.has('value2')).toBeTruthy();
});

it('can remove value from topic tree', () => {
    const tree = new TopicTree({allowMultipleValues : true});
    tree.add('user.{id}', 'value1');
    
    tree.remove('user.{id}', 'value1');

    expect(tree.find('user.remco').firstValue).toBeFalsy();
});

it('cannot add conflicting topics', () => {
    const tree = new TopicTree();

    tree.add('user.{id}', 'value1');
    expect(() => {
        tree.add('user.remco', 'value2');
    }).toThrow();

    tree.add('recipe.1', 'value1');
    expect(() => {
        tree.add('recipe.{id}', 'value2');
    }).toThrow();
});