const TopicTree = require('./topic');
const Realm = require('./realm');
const errors = require('./errors');

const errorTimeoutExceeded = new Error('Timeout exceeded');
const errorNoRealmRegistered = new Error('No realm registered for given id');

const jsonRPCVersion = '2.0';

const rootRealmID = Symbol();

/**
 * Creates a rpc server to handle json rpc messages
 */
module.exports = class RPCServer {

	/**
	 * @param {http.Server} server 
	 * @param {{sendTimeout : Number}} options
	 */
	constructor(server, {sendTimeout = 20e3} = {}) {
		this._server = server;
		this._sendTimeout = sendTimeout;

		this._server.on('connection', conn => {
			conn.on('message', this._handleMessage.bind(this, conn));
			conn.on('close',   this._handleClose.bind(this, conn));
		});

		this._realms = new TopicTree();

		this._registerRootRealm();
	}

	/**
	 * Calls the given method on the given realmId.
	 * realmId can be null to specify the root realm.
	 * 
	 * For example
	 * server.callMethod('/user/remco', 'changeUsername', 'remco2');
	 * 
	 * @param {String} realmId 
	 * @param {String} method 
	 * @param {Array} params 
	 * @returns {Promise}
	 */
	callMethod(realmId, method, ...params) {
		const result = this._getRealm(realmId);
		if(!result) {
			return Promise.reject( errorNoRealmRegistered );
		}
		const {realm, realmData} = result;
		return realm._callMethod(method, realmData, params);
	}

	/**
	 * Declare a realm or return the existing one for the given id
	 * @param {String} id 
	 * @returns {Realm}
	 */
	realm(id) {
		const result = this._realms.find(id);
		if(result) {
			return result.firstValue;
		}
		
		const realm = this._createRealm(id);
		this._realms.add(id, realm);
		return realm;
	}

	/**
	 * @private
	 * @param {String|Symbol} id 
	 */
	_createRealm(id) {
		const serverSend = this._send.bind(this);
		return new Realm({serverSend, id});
	}

	/**
	 * @private
	 */
	_registerRootRealm() {
		this._rootRealm = this._createRealm(rootRealmID);

		for(let method of ['register', 'subscribe']) {
			this[method] = this._rootRealm[method].bind(this._rootRealm);
		};
	}

	/**
	 * @private
	 * @param {*} conn 
	 */
	_handleClose(conn) {
		this._rootRealm._handleClose(conn);

		for(let realm of this._realms.values()) {
			realm._handleClose(conn);
		}
	}

	/**
	 * @private
	 * @param {String} realmId 
	 * @param {*} conn 
	 * @param {*} data 
	 */
	_send(realmId, conn, data) {
		return Promise.race([
			this._timeout(this._sendTimeout),

			new Promise((resolve, reject) => {
				const realm = realmId !== rootRealmID ? realmId : null;
				const body = JSON.stringify( Object.assign({jsonrpc : jsonRPCVersion, realm}, data) );
				conn.send(body, resolve);
			})
		]);
	}

	/**
	 * @private
	 * @param {*} ms 
	 */
	_timeout(ms) {
		return new Promise((resolve, reject) => {
			setTimeout(() => reject(errorTimeoutExceeded), ms).unref();
		});
	}

	/**
	 * Returns the realm and realmData for the given id
	 * @param {String} id 
	 * @returns {{realm : Realm, realmData : {id : String, vars : Object}}}
	 */
	_getRealm(id=null) {
		let realm = this._rootRealm;
		let realmData = {id, vars : null};

		if(id) {
			const result = this._realms.find(id);
			if(result !== null) {
				realm = result.firstValue;
				realmData.vars = result.vars;
			}
			else {
				return null;
			}
		}
		
		return {realm, realmData};
	}

	/**
	 * @private
	 * @param {*} conn 
	 * @param {*} data 
	 */
	_handleMessage(conn, data) {
		let jsonData;
		try {
			jsonData = JSON.parse(data);
		}
		catch(e) {
			return this._send(null, conn, {id : null, result : errors.PARSE_ERROR});
		}

		const {jsonrpc, realm : realmId = null, method, params, id} = jsonData;
		const result = this._getRealm(realmId);

		if(!jsonrpc || !result) {
			return this._send(realmId, conn, {id : null, result : errors.INVALID_REQUEST_ERROR});
		}

		const {realmData, realm} = result;

		realm._handleMessage(conn, {method, params, id, realmData});
	}
}