
const emptySet = new Set();

const wildcard = Symbol();

const errorBranchConflict = new Error('Conflicting branches');
const errorMultipleValuesNotAllowed = new Error('Multiple values not allowed for leaf');
const errorInvalidTopicPart = new Error('Topic parts can match the following characters [a-zA-Z], [0-9], -');

const validTopicPart = /^[\w\-\{\}]+$/;

/**
 * TopicTree accepts "topics" (paths) in the following formats:
 * - user.remco
 * - user.{id}
 * 
 * The following is not allowed
 * - Overwriting wildcard paths. For example first define user.{id} and then define user.remco
 * - Overwriting paths with wilcard paths. For example first define user.remco and then user.{id}
 * - Adding multiple values on the same path (unless you set allowMultipleValues to 'true')
 * 
 * The topic tree provides methods to add a topic and search the tree
 * 
 */
module.exports = class TopicTree {
	constructor({allowMultipleValues = false} = {}) {
		this.root = new TopicBranch();
		this._allowMultipleValues = allowMultipleValues;
	}

	/**
	 * Returns a generator to loop through all the values in the branch
	 * @returns {Generator}
	 */
	*values() {
		for(let branch of this.root.walkBranches()) {
			yield *branch.values;
		}
	}

	/**
	 * Finds the topic and return the values and vars assigned to that topic
	 * @param {String} topic 
	 * @returns {{values : Set, vars : Set<String>}}
	 */
	find(topic) {
		const result = this._findBranch(topic);
		if(result) {
			const values = new Set(result.branch.values);

			return {
				values,
				firstValue : values.size ? Array.from(values)[0] : null,
				vars	   : result.vars,
			}
		}
		return null;
	}

	/**
	 * Returns whether the tree has the given topic
	 * @param {String} topic 
	 * @returns {Boolean}
	 */
	has(topic) {
		let branch = this.root;
		return this._parseTopic(topic).every(part => {
			branch = branch.getBranch(part);
			return branch !== null;
		});
	}

	/**
	 * Adds a value to the given topic. The value can be any type.
	 * Throws an error when you try to add multiple values when it's not allowed
	 * @param {String} topic 
	 * @param {*} value 
	 */
	add(topic, value) {
		let target = this._parseTopic(topic).reduce((branch, part) => {
			return branch.declareBranch(part);
		}, this.root);

		if(!this._allowMultipleValues && target.values.size) {
			throw errorMultipleValuesNotAllowed;
		}

		target.addValue(value);
	}

	/**
	 * Remove the given value from the topic
	 * @param {String} topic 
	 * @param {*} value 
	 */
	remove(topic, value) {
		const result = this._findBranch(topic);
		if(result) {
			result.branch.removeValue(value);
		}
	}

	/**
	 * Remove from the given value from the whole tree
	 * @param {*} value 
	 */
	removeValue(value) {
		for(let branch of this.root.walkBranches()) {
			if(branch.hasValue(value)) {
				branch.removeValue(value);
			}
		}
	}

	/**
	 * Finds the branch for the given topic
	 * @private
	 * @param {String} topic 
	 */
	_findBranch(topic) {
		let vars = new Map();

		let branch = this.root;
		for(let part of this._parseTopic(topic)) {
			branch = branch.getBranch(part);

			if(!branch) {
				return null; //bail early
			}

			if(branch.varName) {
				vars.set(branch.varName, part);
			}
		}

		return {branch, vars};
	}

	/**
	 * Validates and parses the given topic
	 * @param {String} topic 
	 */
	_parseTopic(topic) {
		const topicParts = topic.split('.').filter(part => part !== '');

		this._validateTopicParts(topicParts);

		return topicParts;
	}

	/**
	 * @private
	 * @param {Array.<String>} topicParts 
	 */
	_validateTopicParts(topicParts) {
		const hasInvalidParts = topicParts.some(part => {
			return !part.match(validTopicPart);
		});

		if(hasInvalidParts) {
			throw errorInvalidTopicPart;
		}
	}
}

/**
 * A TopicBranch represents a single branch in the topic tree
 */
class TopicBranch {

	/**
	 * @param {String} varName 
	 */
	constructor(varName) {
		this._varName  = varName;
		this._values   = null;
		this._branches = null;
	}

	/**
	 * Returns all the values in the branch
	 */
	get values() {
		return this._values || emptySet;
	}

	/**
	 * Returns the assigned varName for this branch
	 */
	get varName() {
		return this._varName;
	}

	/**
	 * Returns a generator to recursivly walk through all the branches
	 * @returns {Generator}
	 */
	*walkBranches() {
		if(!this._branches) {
			return;
		}

		for(let branch of this._branches.values()) {
			yield branch;
			yield *branch.walkBranches();
		}
	}

	/**
	 * Returns true when the value is present in this branch
	 * @param {*} value 
	 */
	hasValue(value) {
		return this._values && this._values.has(value);
	}

	/**
	 * Remove the given value from this branch
	 * @param {*} value 
	 */
	removeValue(value) {
		if(!this._values) {
			return;
		}
		this._values.delete(value);
	}

	/**
	 * Adds the given value to this branch
	 * @param {*} value 
	 */
	addValue(value) {
		if(!this._values) {
			this._values = new Set();
		}
		this._values.add(value);
	}

	/**
	 * Gets the child branch with the given name
	 * @param {String} name 
	 */
	getBranch(name) {
		if(!this._branches) {
			return null;
		}

		return this._branches.has(wildcard) 
			? this._branches.get(wildcard) 
			: this._branches.get(name) || null;
	}

	/**
	 * Creates a branch or returns the existing branch with the given name
	 * @param {String} name 
	 */
	declareBranch(name) {
		const {key, varName} = this._parseName(name);

		if(!this._branches) {
			this._branches = new Map();
		}

		if(
			(key !== wildcard && this._branches.has(wildcard)) || 
			(key === wildcard && this._branches.size)) {
			throw errorBranchConflict;
		}

		const existingBranch = this._branches.get(key);
		if(existingBranch && existingBranch.varName === varName) {
			return existingBranch;
		}

		const branch = new TopicBranch(varName);

		this._branches.set(key, branch);

		return branch;
	}

	/**
	 * @private
	 * @param {String} name 
	 */
	_parseName(name) {
		let key = name, varName = null;

		//check if the name is in the following format: {id}
		if(name.startsWith("{") && name.endsWith("}")) {
			key = wildcard;
			varName = name.slice(1,-1);
		}

		return {key,varName};
	}
}