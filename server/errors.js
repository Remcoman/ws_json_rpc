const error = (code, message) => ({code, message});

exports.PARSE_ERROR 		   = error(-32700, "Parse error");
exports.INVALID_REQUEST_ERROR  = error(-32600, "Invalid request");
exports.METHOD_NOT_FOUND_ERROR = error(-32601, "Method not found");

exports.INTERNAL_SERVER_ERROR = (msg) => error(-32603, msg);