
const TopicTree = require('./topic');
const errors = require('./errors');

const realmProp = Symbol();

const errorMethodDoesNotExist = new Error('Method does not exist');

const realmDataProto = {

	/**
	 * Emit a event for subscribers of this realm and event
	 * @param {String} topic the topic of the event
	 * @param {*} data the data that belongs to the event
	 */
	emit(topic, data) {
		const eventData = this[realmProp]._events.find(topic);

		if(!eventData) {
			return;
		}

		const {values, params} = eventData;

		for (let conn of values) {
			this[realmProp]._send(this.id, conn, {id : null, topic, result : data});
		}
	},

	/**
	 * Broadcast a event to all the given connections
	 * @param {Array.<*>} conns 
	 * @param {String} topic 
	 * @param {*} data 
	 */
	broadcast(conns, topic, data) {
		for (let conn of conns) {
			this[realmProp]._send(this.id, conn, {id : null, topic, result : data});
		}
	}
}

/**
 * All rpc messages go to a realm, which by default is the root realm.
 * A realm is used to group relevant messages.
 * 
 * Example:
 * 
 * The user is logged in as username "remco". 
 * You want all rpc methods to only target this user.
 * In the client you create the realm "user.remco" and in the server you listen to "user.{id}"
 * You call methods on this realm from the client and server will automaticly know that those messages
 * belong to user "remco".
 * 
 */
module.exports = class Realm {

	/**
	 * @constructor
	 * @param {{serverSend : Function, id : String}} options
	 */
	constructor({serverSend, id}) {
		this._id   = id;
		this._send = serverSend;

		this._methods 		= new Map();
		this._notifications = new Map();
		this._events 		= new TopicTree({allowMultipleValues : true});

		this.register('rpc.on',  this._handleOn.bind(this));
		this.register('rpc.off', this._handleOff.bind(this));
	}

	/**
	 * Subscribe to the given notification 
	 * @param {String} name 
	 * @param {Function} callback 
	 */
	subscribe(name, callback) {
		this._notifications.set(name, callback);
	}

	/**
	 * Subscribe to a method call
	 * @param {String} name 
	 * @param {Function} callback 
	 */
	register(name, callback) {
		this._methods.set(name, callback);
	}

	/**
	 * Get the id for the realm
	 */
	get id() {
		return this._id;
	}

	/**
	 * Manually call the method with the given name for this realm
	 * @private
	 * @param {String} name 
	 * @param {Object} realmData
	 * @param {Array} params 
	 * @returns {Promise}
	 */
	_callMethod(name, realmData, params) {
		if(!this._methods.has(name)) {
			throw errorMethodDoesNotExist;
		}
		const method = this._methods.get(name);
		return this._exec(method, {conn : null, params, realmData});
	}

	/**
	 * @private
	 * @param {*} conn 
	 */
	_handleClose(conn) {
		this._events.removeValue(conn);
	}

	/**
	 * @private
	 * @param {*} options 
	 */
	_handleOn({conn, params}) {
		let [topic, ] = params;

		if(!topic) {
			throw new Error('no topic given');
		}

		this._events.add(topic, conn);
	}

	/**
	 * @private
	 * @param {*} options 
	 */
	_handleOff({conn, params}) {
		let [topic, ] = params;

		this._events.remove(topic, conn);
	}

	/**
	 * @private
	 * @param {{vars : Object, id : String}} realmData 
	 */
	_createEnhancedRealmData(realmData) {
		const enhancedRealmData = Object.create(realmDataProto);
		Object.assign(enhancedRealmData, realmData);
		enhancedRealmData[realmProp] = this;
		return enhancedRealmData;
	}

	/**
	 * @private
	 * @param {*} conn 
	 * @param {{method : String, params : Object, id : String, realmData : Object}} options
	 */
	_handleMessage(conn, {method, params = null, id = null, realmData = {}}) {
		if(id !== null) {
			return this._handleMethodCall(method, conn, {params, id, realmData});
		}
		else {
			return this._handleNotification(method, conn, {params, realmData});
		}
	}

	/**
	 * @private
	 * @param {*} name 
	 * @param {*} conn 
	 * @param {{params : String, id : String, realmData : Object}} options
	 */
	_handleMethodCall(name, conn, {params, id, realmData}) {
		if(!this._methods.has(name)) {
			return this._send(realmData.id, conn, {id : null, result : errors.METHOD_NOT_FOUND_ERROR});
		}

		//execute the method
		return this._exec(this._methods.get(name), {conn, params, realmData})

			//succesful exec so send result
			.then(result => {
				return this._send(realmData.id, conn, {id, result});
			})

			//error when executing method. Send error response
			.catch(error => {
				return this._send(realmData.id, conn, {id, error : errors.INTERNAL_SERVER_ERROR(error.message)});
			})
	}

	/**
	 * @private
	 * @param {*} name 
	 * @param {*} conn 
	 * @param {{params : String, realmData : Object}} options
	 */
	_handleNotification(name, conn, {params, realmData}) {
		if(!this._notifications.has(name)) {
			return this._send(realmData.id, conn, {id : null, result : errors.METHOD_NOT_FOUND_ERROR});
		}

		return this._exec(this._notifications.get(name), {conn, params, realmData});
	}

	/**
	 * @private
	 * @param {Function} fn
	 * @param {{conn : *, params : Array, realmData : Object}}
	 */
	_exec(fn, {conn, params, realmData}) {
		let result;
		
		try {
			result = fn({conn, params, realmData : this._createEnhancedRealmData(realmData)});
		}
		catch(e) {
			return Promise.reject(result);
		}

		return Promise.resolve(result);
	}
}