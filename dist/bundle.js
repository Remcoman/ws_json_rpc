'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var nanoid = _interopDefault(require('nanoid'));

function after(reject, ms) {
	var t = window.setTimeout(reject, ms);

	return {
		cancel: function cancel() {
			window.clearTimeout(t);
		}
	}
}

var errorTimeout = new Error('timeout occured when calling method');

/**
 * A realm groups common rpc calls by the given id.
 *
 * For example you can create a realm for a user like this:
 *
 * realm = client.realm('user.remco')
 * realm.call('get_address')
 *
 * a server should have its realm declared like this:
 *
 * realm = server.realm('user.{id}')
 * 
 */
var RPCClientRealm = function RPCClientRealm(ref) {
	var id = ref.id;
	var socketSend = ref.socketSend;
	var callTimeout = ref.callTimeout;

	this._id = id;
	this._socketSend = socketSend;
	this._callTimeout = callTimeout;
	this._subscribed = new Map();
	this._callbacks  = new Map();
};

/**
	 * Send a notification to the server
	 * The server needs to subscribe to this event to receive it
	 * @param {String} event 
	 * @param {*} params 
	 */
RPCClientRealm.prototype.notify = function notify (event) {
		var params = [], len = arguments.length - 1;
		while ( len-- > 0 ) params[ len ] = arguments[ len + 1 ];

	this._send({method : event, id : null, params: params});
		
	return Promise.resolve();
};

/**
	 * Call the given method on the server for this realm
	 * @param {String} method 
	 * @param {*} params 
	 */
RPCClientRealm.prototype.call = function call (method) {
		var this$1 = this;
		var params = [], len = arguments.length - 1;
		while ( len-- > 0 ) params[ len ] = arguments[ len + 1 ];

	return new Promise(function (resolve, reject) {
		var id = nanoid();

		this$1._callbacks.set(id, {
			resolve: resolve, 
			reject: reject,
			timeout : after(this$1._timeout.bind(this$1, id), this$1._callTimeout)
		});

		this$1._send({method: method, id: id, params: params});
	});
};

/**
	 * Unsubscribe for the given topic callback combination
	 * @param {String} topic 
	 * @param {Function} callback 
	 * @returns {Promise}
	 */
RPCClientRealm.prototype.unsubscribe = function unsubscribe (topic, callback) {
		var this$1 = this;

	return this.call('rpc.off', topic).then(function () {
		this$1._subscribed.get(topic).delete(callback);
	});
};

/**
	 * Subscribe for the given topic
	 * @param {String} topic 
	 * @param {Function} callback 
	 */
RPCClientRealm.prototype.subscribe = function subscribe (topic, callback) {
		var this$1 = this;

	return this.call('rpc.on', topic).then(function () {
		if(this$1._subscribed.has(topic)) {
			this$1._subscribed.get(topic).add(callback);
		}
		else {
			this$1._subscribed.set(topic, new Set([callback]));
		}
	});
};

RPCClientRealm.prototype._timeout = function _timeout (id) {
	var ref = this._callbacks.get(id);
		var reject = ref.reject;
	this._callbacks.delete(id);
	reject(errorTimeout);
};

/**
	 * @private
	 * @param {*} data 
	 */
RPCClientRealm.prototype._send = function _send (data) {
	data = Object.assign({jsonrpc : "2.0", realm : this._id}, data);

	this._socketSend(JSON.stringify(data));
};

/**
	 * @private
	 * @param {{id : String, topic : String, error : Error, result : *}} options 
	 */
RPCClientRealm.prototype._handleMessage = function _handleMessage (ref) {
		var id = ref.id;
		var topic = ref.topic;
		var error = ref.error;
		var result = ref.result;

	if(id === null) {
		if(error) { //generic error
			topic = 'error';
		}

		if(!this._subscribed.has(topic)) {
			return //silently ignore non registered topics
		}
			
		this._subscribed.get(topic).forEach(function (callback) { return callback(result); });
	}
	else if(this._callbacks.has(id)) {
		if(error !== null) {
			this._callbacks.get(id).reject(new Error(error.message));
		}
		else {
			this._callbacks.get(id).resolve(result);
		}

		var ref$1 = this._callbacks.get(id);
			var timeout = ref$1.timeout;
		timeout.cancel();

		this._callbacks.delete(id);
	}
};

function defaultBackoffFunction(delay) {
	return Math.min(delay * 3, 10 * 60 * 1000);
}

var RPCConnector = function RPCConnector(url, opts) {
	if ( opts === void 0 ) opts = {reconnect : true, backOff : defaultBackoffFunction};

	this._url = url;
	this._opts = opts;
	this._closed = false;
	this._eventTarget = document.createElement("div");
	this._reconnectTimeout = 0;
	this._buffer = [];
	this._backoffDelay = 0;

	this.addEventListener = this._eventTarget.addEventListener.bind(this._eventTarget);
	this.removeEventListener = this._eventTarget.removeEventListener.bind(this._eventTarget);
	this.dispatchEvent = this._eventTarget.dispatchEvent.bind(this._eventTarget);
	this.onopen = this.onmessage = this.onclose = null;

	this._initConnection();
};

/**
	 * Close the current connection 
	 * All buffered messages are discarded and reconnections are canceled
	 */
RPCConnector.prototype.close = function close () {
	if(this._socket) {
		this._socket.close();
	}
	else if(this._opts.reconnect) {
		clearTimeout(this._reconnectTimeout);
	}

	this._closed = true;
	this._buffer.length = 0;
};

/**
	 * Sends a message through the current socket
	 * Messages are pushed into the buffer when no connection was yet established 
	 * 
	 * @param  {*} msg
	 */
RPCConnector.prototype.send = function send (msg) {
	if(this._closed) {
		throw new Error("Connector was closed and can't receive messages anymore");
	}

	if(!this._socket || this._socket.readyState !== this.OPEN) {
		this._buffer.push(msg);
	}
	else {
		this._socket.send(msg);
	}
};

/**
	 * @private
	 * @param  {Event}
	 * @return {Event}
	 */
RPCConnector.prototype._cloneEvent = function _cloneEvent (e) {
	return new (e.constructor)(e.type, e);
};

/**
	 * @private
	 */
RPCConnector.prototype._initConnection = function _initConnection () {
		var this$1 = this;

	this._socket = new WebSocket(this._url);

	this._socket.addEventListener('open', function (e) {
		this$1._backoffDelay = 0;

		while(this$1._buffer.length) {
			this$1.send( this$1._buffer.shift() );
		}

		e = this$1._cloneEvent(e);

		this$1.dispatchEvent(e);

		if(typeof this$1.onopen === "function") {
			this$1.onopen(e);
		}
	});

	this._socket.addEventListener('close', function (e) {
		this$1._socket = null;

		if(this$1._closed || e.code === 1000 || e.code === 1001) {
			e = this$1._cloneEvent(e);

			this$1.dispatchEvent(e);

			if(typeof this$1.onclose === "function") {
				this$1.onclose(e);
			}
		}
		else if(this$1._opts.reconnect) {
			this$1._backoffDelay = this$1._opts.backOff(this$1._backoffDelay);
			this$1._reconnectTimeout = setTimeout(this$1._initConnection.bind(this$1), this$1._backoffDelay);
		}
	});

	this._socket.addEventListener('message', function (e) {
		e = this$1._cloneEvent(e);

		this$1.dispatchEvent(e);

		if(typeof this$1.onmessage === "function") {
			this$1.onmessage(e);
		}
	});
};

Object.assign(RPCConnector.prototype, {
	CONNECTING : 0,
	OPEN 	   : 1,
	CLOSING    : 2,
	CLOSED 	   : 3
});

var defaultCallTimeout = 10000;

var RPCClient = function RPCClient(socket, ref) {
	if ( ref === void 0 ) ref = {};
	var callTimeout = ref.callTimeout; if ( callTimeout === void 0 ) callTimeout = defaultCallTimeout;

	if(!socket) {
		throw new Error('Socket is required');
	}

	if(!socket.send || !socket.addEventListener || !socket.removeEventListener) {
		throw new Error('Socket should at least implement a send, addEventListener and removeEventListener method');
	}

	this._callTimeout = callTimeout;

	this._handleMessageBound = this._handleMessage.bind(this);

	this._socket = socket;
	this._socket.addEventListener("message", this._handleMessageBound, false);

	this._realms = new Map();

	this._registerRootRealm();
};

/**
	 * Destroys the client and stops listening to the socket
	 */
RPCClient.prototype.destroy = function destroy () {
	this._socket.removeEventListener("message", this._handleMessageBound, false);
	this._socket = null;
	this._realms.clear();
};

/**
	 * Declares a new realm with the given id. 
	 * An existing realm is returned if it already exists.
	 *
	 * A realm groups common rpc requests by the given id.
	 * 
	 * @param  {String} id
	 * @return {RPCClientRealm}
	 */
RPCClient.prototype.realm = function realm (id) {
	var socketSend = this._socket.send.bind(this._socket);

	if(this._realms.has(id)) {
		return this._realms.get(id);
	}

	var realm = new RPCClientRealm({id: id, socketSend: socketSend, callTimeout : this._callTimeout});

	this._realms.set(id, realm);

	return realm;
};

/**
	 * @private
	 */
RPCClient.prototype._registerRootRealm = function _registerRootRealm () {
		var this$1 = this;

	this._rootRealm = this.realm('');

	(['notify', 'call', 'unsubscribe', 'subscribe']).forEach(function (method) {
		this$1[method] = this$1._rootRealm[method].bind(this$1._rootRealm);
	});
};

/**
	 * @private
	 * @param  {Object}
	 */
RPCClient.prototype._handleMessage = function _handleMessage (ref) {
		var data = ref.data;

	var jsonData;
	try {
		jsonData = JSON.parse(data);
	}
	catch(e) {
		console.warn('Got invalid json data from server');
		return;
	}

	var topic = jsonData.topic; if ( topic === void 0 ) topic = null;
		var error = jsonData.error; if ( error === void 0 ) error = null;
		var result = jsonData.result; if ( result === void 0 ) result = null;
		var id = jsonData.id; if ( id === void 0 ) id = null;
		var realmId = jsonData.realm; if ( realmId === void 0 ) realmId = null;

	var realm = realmId !== null 
		? this._realms.get(realmId)
		: this._rootRealm;

	if(!realm) {
		console.warn(("Got message for realm " + realmId + " which does not exist"));
		return;
	}

	realm._handleMessage({topic: topic, error: error, result: result, id: id});
};

exports['default'] = RPCClient;
exports.RPCConnector = RPCConnector;
