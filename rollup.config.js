import buble from 'rollup-plugin-buble';

export default {
    input: 'client/index.js',
    external : ['nanoid'],
    output: {
        exports: 'named',
        file: 'dist/bundle.js',
        format: 'cjs'
    },
    plugins: [ 
        buble()
    ]
};