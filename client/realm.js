import nanoid from 'nanoid';

function after(reject, ms) {
	const t = window.setTimeout(reject, ms);

	return {
		cancel() {
			window.clearTimeout(t);
		}
	}
}

const errorTimeout = new Error('timeout occured when calling method');

/**
 * A realm groups common rpc calls by the given id.
 *
 * For example you can create a realm for a user like this:
 *
 * realm = client.realm('user.remco')
 * realm.call('get_address')
 *
 * a server should have its realm declared like this:
 *
 * realm = server.realm('user.{id}')
 * 
 */
export default class RPCClientRealm {

	/**
	 * @constructor
	 * @param {{id : String, socketSend : Function, callTimeout : Number}}
	 */
	constructor({id, socketSend, callTimeout}) {
		this._id = id;
		this._socketSend = socketSend;
		this._callTimeout = callTimeout;
		this._subscribed = new Map();
		this._callbacks  = new Map();
	}

	/**
	 * Send a notification to the server
	 * The server needs to subscribe to this event to receive it
	 * @param {String} event 
	 * @param {*} params 
	 */
	notify(event, ...params) {
		this._send({method : event, id : null, params});
		
		return Promise.resolve();
	}

	/**
	 * Call the given method on the server for this realm
	 * @param {String} method 
	 * @param {*} params 
	 */
	call(method, ...params) {
		return new Promise((resolve, reject) => {
			const id = nanoid();

			this._callbacks.set(id, {
				resolve, 
				reject,
				timeout : after(this._timeout.bind(this, id), this._callTimeout)
			});

			this._send({method, id, params});
		});
	}

	/**
	 * Unsubscribe for the given topic callback combination
	 * @param {String} topic 
	 * @param {Function} callback 
	 * @returns {Promise}
	 */
	unsubscribe(topic, callback) {
		return this.call('rpc.off', topic).then(() => {
			this._subscribed.get(topic).delete(callback);
		});
	}

	/**
	 * Subscribe for the given topic
	 * @param {String} topic 
	 * @param {Function} callback 
	 */
	subscribe(topic, callback) {
		return this.call('rpc.on', topic).then(() => {
			if(this._subscribed.has(topic)) {
				this._subscribed.get(topic).add(callback);
			}
			else {
				this._subscribed.set(topic, new Set([callback]));
			}
		});
	}

	_timeout(id) {
		const {reject} = this._callbacks.get(id);
		this._callbacks.delete(id);
		reject(errorTimeout);
	}

	/**
	 * @private
	 * @param {*} data 
	 */
	_send(data) {
		data = Object.assign({jsonrpc : "2.0", realm : this._id}, data);

		this._socketSend(JSON.stringify(data));
	}

	/**
	 * @private
	 * @param {{id : String, topic : String, error : Error, result : *}} options 
	 */
	_handleMessage({id, topic, error, result}) {
		if(id === null) {
			if(error) { //generic error
				topic = 'error';
			}

			if(!this._subscribed.has(topic)) {
				return //silently ignore non registered topics
			}
			
			this._subscribed.get(topic).forEach(callback => callback(result));
		}
		else if(this._callbacks.has(id)) {
			if(error !== null) {
				this._callbacks.get(id).reject(new Error(error.message));
			}
			else {
				this._callbacks.get(id).resolve(result);
			}

			const {timeout} = this._callbacks.get(id);
			timeout.cancel();

			this._callbacks.delete(id);
		}
	}
}