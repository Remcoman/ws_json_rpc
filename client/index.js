import RPCClientRealm from './realm';

export {default as RPCConnector} from './connector';

const defaultCallTimeout = 10000;

export default class RPCClient {

	/**
	 * Creates a new RPCClient
	 * 
	 * @param  {WebSocket|RPCConnector}
	 */
	constructor(socket, {callTimeout = defaultCallTimeout} = {}) {
		if(!socket) {
			throw new Error('Socket is required');
		}

		if(!socket.send || !socket.addEventListener || !socket.removeEventListener) {
			throw new Error('Socket should at least implement a send, addEventListener and removeEventListener method');
		}

		this._callTimeout = callTimeout;

		this._handleMessageBound = this._handleMessage.bind(this);

		this._socket = socket;
		this._socket.addEventListener("message", this._handleMessageBound, false);

		this._realms = new Map();

		this._registerRootRealm();
	}

	/**
	 * Destroys the client and stops listening to the socket
	 */
	destroy() {
		this._socket.removeEventListener("message", this._handleMessageBound, false);
		this._socket = null;
		this._realms.clear();
	}

	/**
	 * Declares a new realm with the given id. 
	 * An existing realm is returned if it already exists.
	 *
	 * A realm groups common rpc requests by the given id.
	 * 
	 * @param  {String} id
	 * @return {RPCClientRealm}
	 */
	realm(id) {
		const socketSend = this._socket.send.bind(this._socket);

		if(this._realms.has(id)) {
			return this._realms.get(id);
		}

		const realm = new RPCClientRealm({id, socketSend, callTimeout : this._callTimeout});

		this._realms.set(id, realm);

		return realm;
	}

	/**
	 * @private
	 */
	_registerRootRealm() {
		this._rootRealm = this.realm('');

		(['notify', 'call', 'unsubscribe', 'subscribe']).forEach(method => {
			this[method] = this._rootRealm[method].bind(this._rootRealm);
		});
	}

	/**
	 * @private
	 * @param  {Object}
	 */
	_handleMessage({data}) {
		let jsonData;
		try {
			jsonData = JSON.parse(data);
		}
		catch(e) {
			console.warn('Got invalid json data from server');
			return;
		}

		let {
			jsonrpc, 
			topic = null, 
			error = null, 
			result = null, 
			id = null,
			realm : realmId = null
		} = jsonData;

		const realm = realmId !== null 
			? this._realms.get(realmId)
			: this._rootRealm;

		if(!realm) {
			console.warn(`Got message for realm ${realmId} which does not exist`);
			return;
		}

		realm._handleMessage({topic, error, result, id});
	}
	
}

