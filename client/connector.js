function defaultBackoffFunction(delay) {
	return Math.min(delay * 3, 10 * 60 * 1000);
}

export default class RPCConnector {

	/**
	 * RPCConnector wraps a WebSocket and provides the following extra features
     *
	 * - Reconnect when the connection was closed due to a connection error
	 * - Backoff is used in combination with reconnect to define the delay after which a reconnect takes place
	 * - Buffering of messages when a connection was not established yet
	 *	
	 * @param  {String} url
	 * @param  {reconnect : Boolean, backOff : Function}
	 */
	constructor(url, opts = {reconnect : true, backOff : defaultBackoffFunction}) {
		this._url = url;
		this._opts = opts;
		this._closed = false;
		this._eventTarget = document.createElement("div");
		this._reconnectTimeout = 0;
		this._buffer = [];
		this._backoffDelay = 0;

		this.addEventListener = this._eventTarget.addEventListener.bind(this._eventTarget);
		this.removeEventListener = this._eventTarget.removeEventListener.bind(this._eventTarget);
		this.dispatchEvent = this._eventTarget.dispatchEvent.bind(this._eventTarget);
		this.onopen = this.onmessage = this.onclose = null;

		this._initConnection();
	}

	/**
	 * Close the current connection 
	 * All buffered messages are discarded and reconnections are canceled
	 */
	close() {
		if(this._socket) {
			this._socket.close();
		}
		else if(this._opts.reconnect) {
			clearTimeout(this._reconnectTimeout);
		}

		this._closed = true;
		this._buffer.length = 0;
	}

	/**
	 * Sends a message through the current socket
	 * Messages are pushed into the buffer when no connection was yet established 
	 * 
	 * @param  {*} msg
	 */
	send(msg) {
		if(this._closed) {
			throw new Error("Connector was closed and can't receive messages anymore");
		}

		if(!this._socket || this._socket.readyState !== this.OPEN) {
			this._buffer.push(msg);
		}
		else {
			this._socket.send(msg);
		}
	}

	/**
	 * @private
	 * @param  {Event}
	 * @return {Event}
	 */
	_cloneEvent(e) {
		return new (e.constructor)(e.type, e);
	}

	/**
	 * @private
	 */
	_initConnection() {
		this._socket = new WebSocket(this._url);

		this._socket.addEventListener('open', e => {
			this._backoffDelay = 0;

			while(this._buffer.length) {
				this.send( this._buffer.shift() );
			}

			e = this._cloneEvent(e);

			this.dispatchEvent(e);

			if(typeof this.onopen === "function") {
				this.onopen(e);
			}
		});

		this._socket.addEventListener('close', e => {
			this._socket = null;

			if(this._closed || e.code === 1000 || e.code === 1001) {
				e = this._cloneEvent(e);

				this.dispatchEvent(e);

				if(typeof this.onclose === "function") {
					this.onclose(e);
				}
			}
			else if(this._opts.reconnect) {
				this._backoffDelay = this._opts.backOff(this._backoffDelay);
				this._reconnectTimeout = setTimeout(this._initConnection.bind(this), this._backoffDelay);
			}
		});

		this._socket.addEventListener('message', e => {
			e = this._cloneEvent(e);

			this.dispatchEvent(e);

			if(typeof this.onmessage === "function") {
				this.onmessage(e);
			}
		});
	}
}

Object.assign(RPCConnector.prototype, {
	CONNECTING : 0,
	OPEN 	   : 1,
	CLOSING    : 2,
	CLOSED 	   : 3
});